#!/usr/bin/python3

from tkinter import *
from tkinter import ttk
import threading
import time
import serial
import io
import sys

tty = sys.argv[1]


root = Tk()

lbltext = StringVar()
com = ["run"]
runline = [0, 0]

def produce_steam(n, sup, sdn):
    with serial.Serial(tty) as ser:
        time.sleep(3) # wait for Serial ?
        while n > 1 and com[0] == "run":
            n = n - 1
            lbltext.set("run: " + str(n))
            ser.write(b'B')
            ser.flush()
            time.sleep(sup)
            ser.write(b'A')
            ser.flush()
            time.sleep(sdn)
    root.event_generate("<<stop>>")
    com[0] = "stop"

def stop(btn, lbl):
    com[0] = "stop"
    btn.grid_forget()
    lbl.grid_forget()

def start():
    try:
        n = int(ntimes.get())
        sup = int(secs_on.get())
        sdn = int(secs_off.get())

        com[0] = "run"
        lbltext.set("running")
        lbl = ttk.Label(mainframe, textvariable=lbltext)
        lbl.grid(column=0, columnspan=2, row=2)
        btn = ttk.Button(mainframe, text="stop", command=lambda: stop(btn, lbl))
        btn.grid(column=2, row=2)
        runline[0] = btn
        runline[1] = lbl
        steamproc = threading.Thread(target=produce_steam, args=(n + 1, sup, sdn))
        steamproc.start()

    except ValueError:
        pass

def cleaner(*args):
    for w in runline:
        w.grid_forget()

root.title("Steamer on " + tty)

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

ttk.Label(mainframe, text="n").grid(column=0, row=0)
ntimes = StringVar()
ntimes_entry = ttk.Entry(mainframe, width=3, textvariable=ntimes)
ntimes_entry.grid(column=0, row=1, sticky=(W, E))

ttk.Label(mainframe, text="seconds up").grid(column=1, row=0)
secs_on = StringVar()
secs_on_entry = ttk.Entry(mainframe, width=5, textvariable=secs_on)
secs_on_entry.grid(column=1, row=1)

ttk.Label(mainframe, text="seconds down").grid(column=2, row=0)
secs_off = StringVar()
secs_off_entry = ttk.Entry(mainframe, width=5, textvariable=secs_off)
secs_off_entry.grid(column=2, row=1)

ttk.Button(mainframe, text="start", command=start).grid(column=3, row=1, sticky=W)

for child in mainframe.winfo_children():
    child.grid_configure(padx=5, pady=5)

root.bind("<<stop>>", cleaner)

root.mainloop()
