#include <Keyboard.h> 
#include "secret.h"
#include "password.h"

const int buttonSecretPin = 2;  
int buttonSecretState = 0;
const int buttonPasswordPin = 4;
int buttonPasswordState = 0;

void setup() {
  pinMode(buttonSecretPin, INPUT_PULLUP);
  pinMode(buttonPasswordPin, INPUT_PULLUP);
  
  //unfortunately LED_BUILTIN does not exist on Micro Pro,
  //so this is useless:
  pinMode(LED_BUILTIN, OUTPUT);

  //to disable TX indicator:
  pinMode(LED_BUILTIN_TX,INPUT);
  
  //we use RX indicator for instant blink:
  pinMode(LED_BUILTIN_RX,OUTPUT);

  Keyboard.begin();

}
void loop() {
  delay(200);
  buttonSecretState = digitalRead(buttonSecretPin);
  buttonPasswordState = digitalRead(buttonPasswordPin);
  if (buttonSecretState == LOW) { 
    //notice how LOW and HIGH are inverted on RX
    digitalWrite(LED_BUILTIN_RX, LOW); 
    delay(5);
    digitalWrite(LED_BUILTIN_RX, HIGH);
    Keyboard.println(secret); 
    delay(3000);
  }
  if (buttonPasswordState == LOW) {
    //notice how LOW and HIGH are inverted on RX
    digitalWrite(LED_BUILTIN_RX, LOW); 
    delay(5);
    digitalWrite(LED_BUILTIN_RX, HIGH);
    Keyboard.println(password); 
    delay(3000);
  }
    
}
