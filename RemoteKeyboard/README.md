Remote keyboard
===============

## Using with TIO

    tio -b 9600 -m ODELBS /dev/ttyUSB0

## Working keys

1. Enter

1. Backspace

1. TAB

1. Ctrl-C

1. Ctrl-D

1. Ctrl-B maps to LeftArrow

1. Ctrl-F maps to RightArrow

1. `\\` maps to \

1. \u maps to UpArrow

1. \d maps to DownArrow

1. ESC

1. \n maps to Fn with n<10

1. \c presses and holds CTRL

1. \a presses and holds ALT

1. \SPACE releases all modifier keys

