#include <Keyboard.h>

void setup() {
  // put your setup code here, to run once:
  Serial1.begin(9600);
  Keyboard.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial1.available() > 0) {
    char c = Serial1.read();
    switch (c) {
      case 10:
        Keyboard.write(KEY_RETURN);
        break;
      case 13:
        Keyboard.write(KEY_RETURN);
        break;
      case 8:
        Keyboard.write(KEY_BACKSPACE);
        break;
      case 3:
        Keyboard.press(KEY_LEFT_CTRL);
        delay(10);
        Keyboard.print('c');
        delay(10);
        Keyboard.release(KEY_LEFT_CTRL);
        break;
      case 4:
        Keyboard.press(KEY_LEFT_CTRL);
        delay(10);
        Keyboard.print('d');
        delay(10);
        Keyboard.release(KEY_LEFT_CTRL);
        break;
      case 2:
        Keyboard.write(KEY_LEFT_ARROW);
        break;
      case 6:
        Keyboard.write(KEY_RIGHT_ARROW);
        break;
      case 27:
        Keyboard.write(KEY_ESC);
        break;
      case 92:
        while (Serial1.available() <= 0) {}
        {
          char afterslash = Serial1.read();
          switch (afterslash) {
            case 92: 
              Keyboard.print('\\');
              break;
            case 49:
              Keyboard.write(KEY_F1);
              break;
            case 50:
              Keyboard.write(KEY_F2);
              break;
            case 51:
              Keyboard.write(KEY_F3);
              break;
            case 52:
              Keyboard.write(KEY_F4);
              break;             
            case 53:
              Keyboard.write(KEY_F5);
              break;
            case 54:
              Keyboard.write(KEY_F6);
              break;
            case 55:
              Keyboard.write(KEY_F7);
              break;             
            case 56:
              Keyboard.write(KEY_F8);
              break;
            case 57:
              Keyboard.write(KEY_F9);
              break;
            case 100:
              Keyboard.write(KEY_DOWN_ARROW);
              break;
            case 117:
              Keyboard.write(KEY_UP_ARROW);
              break;
            case 99:
              Keyboard.press(KEY_LEFT_CTRL);
              break;
            case 97:
              Keyboard.press(KEY_LEFT_ALT);
              break;
            case 32:
              Keyboard.releaseAll();
              break; 
            default:
              delay(10);
            }
        }
        break;
      default:
        Keyboard.print(c);
    }
  }


  
}
